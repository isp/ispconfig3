[![coverage report](https://git.ispconfig.org/isp/ispconfig3/badges/master/coverage.svg)](https://git.ispconfig.org/isp/ispconfig3/commits/master)




# ISPConfig - Hosting Control Panel

would NSA use it to administrate their lil network ?




- Manage multiple servers from one control panel
- Web server management (Apache2 and nginx)
- Mail server management (with virtual mail users)
- DNS server management (BIND and MyDNS)
- Virtualization (OpenVZ)
- Administrator, reseller and client login
- Configuration mirroring and clusters
- Open Source software (BSD license)